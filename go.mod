module gitlab.com/dacnpm.f88/heart-rate-server

go 1.16

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-akka/configuration v0.0.0-20200606091224-a002c0330665 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.2.2
	github.com/labstack/gommon v0.3.0
	github.com/lib/pq v1.10.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.8
)
