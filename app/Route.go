package app

import (
	"github.com/labstack/echo/v4"
	log "github.com/labstack/gommon/log"
	"gitlab.com/dacnpm.f88/heart-rate-server/controller"
	mdw "gitlab.com/dacnpm.f88/heart-rate-server/middleware"
)

func defineAPIRoutes(e *echo.Echo) {
	e.GET("/", controller.Home)

	e.POST("/register", controller.Register)
	e.POST("/login", controller.Login)
	e.GET("/renew-token/:user-id", controller.RenewToken)

	user := e.Group("/user", mdw.ValidateAuthentication)
	user.GET("/all", controller.GetAllUsers)
	user.GET("/:id", controller.GetUserById, mdw.CheckUser)
	user.PUT("/:id", controller.UpdateUser, mdw.CheckUser)
	user.DELETE("/:id", controller.DeleteUser, mdw.CheckUser)
	user.POST("/:id/result", controller.AddUserResult, mdw.CheckUser)
	user.GET("/:id/result/all", controller.GetAllUserResults, mdw.CheckUser)
}

func Routes(e *echo.Echo) {
	// Define all routes of API here
	defineAPIRoutes(e)

	// Print all the routes of API on
	allRoutes := e.Routes()
	for i := 0; i < len(allRoutes); i++ {
		route := allRoutes[i]
		log.Infof("%s %s", route.Method, route.Path)
	}
}
