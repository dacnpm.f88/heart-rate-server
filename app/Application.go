package app

import (
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	mdw "gitlab.com/dacnpm.f88/heart-rate-server/middleware"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	echoInstance *echo.Echo
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)
}

func getEchoInstance() *echo.Echo {
	if echoInstance == nil {
		echoInstance = echo.New()

		// Setting middleware for all routes
		mdw.UseMdw(echoInstance)

		// Define route for API
		Routes(echoInstance)
	}

	return echoInstance
}

func startRestServer() {
 	e := getEchoInstance()
	listenAddr := os.Getenv("LISTEN_ADDR")
	listenPort := os.Getenv("PORT")
	requestTimeout := 10

	s := &http.Server{
		Addr:         listenAddr + ":" + listenPort,
		ReadTimeout:  time.Duration(requestTimeout) * time.Second,
		WriteTimeout: time.Duration(requestTimeout) * time.Second,
	}

	go e.Logger.Fatal(e.StartServer(s))
}

func Start() {
	startRestServer()
}