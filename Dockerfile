FROM golang:alpine3.13
RUN mkdir /build
WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o main .
CMD ["./main"]
EXPOSE 8080
EXPOSE 443
