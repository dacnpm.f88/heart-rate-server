## Heart Rate Server
<hr>

### Run application
1. Run the services:
```
> docker build -t heart-rate-server:dev .
> docker-compose up
```
2. Go to Browser or Postman and visit:
```
http://localhost:8080
```
### Manually build source code
In the root directory of the project, run:
```
> go mod tidy
> go run main.go
```

### References
* [A Clean Architecture For Web Application In Golang](https://medium.com/wesionary-team/a-clean-architecture-for-web-application-in-go-lang-4b802dd130bb)
* [CRUD RESTful API with Go, GORM, JWT, Postgres, Mysql, and Testing](https://levelup.gitconnected.com/crud-restful-api-with-go-gorm-jwt-postgres-mysql-and-testing-460a85ab7121)
* [Dockerizing a CRUD RESTful API with Go](https://levelup.gitconnected.com/dockerized-crud-restful-api-with-go-gorm-jwt-postgresql-mysql-and-testing-61d731430bd8)
* [Dockerize Project NodeJS, MongoDB, Redis, Passport](https://viblo.asia/p/dockerize-project-nodejs-mongodb-redis-passport-4P856NXW5Y3)