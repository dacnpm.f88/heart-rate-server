package main

import (
	"gitlab.com/dacnpm.f88/heart-rate-server/app"
)

func main() {
	app.Start()
}
