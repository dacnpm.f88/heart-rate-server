#### 1. Add new user
* End point: **<BASE_URL>/user**
* Method: POST
* Request header:
* Request body: 
```
{
    "username": "Phan Chi Sac",
    "email": "sacphan.01@gmail.com",
    "password": "202020"
}
```
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 6,
        "username": "Phan Chi Sac",
        "email": "sacphan.01@gmail.com",
        "password": "$2a$10$a5p030h6OfKAvPNrBecgueH1tFKRG4PDVJVU/rCOU5OqhtveiWs4W",
        "results": null,
        "created_at": "2021-05-01T21:07:35.885878+07:00",
        "updated_at": "2021-05-01T21:07:35.885878+07:00"
    }
}
```

#### 2. Get User By ID
* End point: **<BASE_URL>/user/:id**
* Method: GET
* Path params:
    * id: <user's id>
* Request header:
  - **x-access-token**: Bearer <access_token>
* Request body:
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 4,
        "username": "Do Tan Tai",
        "email": "dotantai@gmail.com",
        "password": "$2a$10$7NiuI8dz5xfiAmx.8gFS1uutYSufgnLj4PyA6RsMrwvvUiTDDrdf.",
        "results": [
            {
                "id": 2,
                "user_id": 4,
                "value": 89,
                "duration": 60,
                "note": "My heart rate",
                "created_at": "2021-05-03T18:56:32.143993+07:00"
            },
            {
                "id": 3,
                "user_id": 4,
                "value": 87,
                "duration": 60,
                "note": "My heart rate",
                "created_at": "2021-05-03T18:56:32.143993+07:00"
            }
        ],
        "created_at": "2021-05-01T20:53:45.933983+07:00",
        "updated_at": "2021-05-01T21:04:56.012413+07:00"
    }
}
```

#### 3. Update User
* End point: **<BASE_URL>/user/:id**
* Method: PUT
* Path params:
    * id: <user's ID>
* Request header:
  - **x-access-token**: Bearer <access_token>
* Request body:
```
{
    "username": "Tran Phi Hung 123",
    "email": "tranphihung@gmail.com",
    "password": "12345"
}
```  
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 2,
        "username": "Tran Phi Hung 123",
        "email": "tranphihung@gmail.com",
        "password": "$2a$10$ts6JIxLYsQ/0ntAmk3zvWO4JSwVrZyFzAxTtVDXES7NWQhmeuUBpG",
        "results": null,
        "created_at": "2021-05-01T21:54:03.060856+07:00",
        "updated_at": "2021-05-01T21:54:03.060856+07:00"
    }
}
```
#### 4. Add user's measurement result
* End point: **<BASE_URL>/user/:id/result**
* Method: POST
* Path params:
  * id: <user's ID>
* Request header:
  - **x-access-token**: Bearer <access_token>
* Request body:
```
{
    "value": 87,
    "note": "My heart rate"
}
```  
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 3,
        "user_id": 4,
        "value": 87,
        "duration": 60,
        "note": "My heart rate",
        "created_at": "2021-05-03T18:56:32.143993+07:00"
    }
}
```
#### 5. Get user's measurement results
* End point: **<BASE_URL>/user/:id/result/all**
* Method: GET
* Path params:
  * id: <user's ID>
* Request header:
  - **x-access-token**: Bearer <access_token>
* Request body:
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": [
        {
            "id": 2,
            "user_id": 4,
            "value": 89,
            "duration": 60,
            "note": "My heart rate",
            "created_at": "2021-05-03T18:56:32.143993+07:00"
        },
        {
            "id": 3,
            "user_id": 4,
            "value": 87,
            "duration": 60,
            "note": "My heart rate",
            "created_at": "2021-05-03T18:56:32.143993+07:00"
        }
    ]
}
```

#### 6. Register
* End point: **<BASE_URL>/register**
* Method: POST
* Path params:
* Request header:
* Request body:
```
{
    "username": "Nguyen Huu Luong Anh",
    "email": "nguyenhuuluonganh@gmail.com",
    "password": "888888"
}
```
* Response body:
```
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 14,
        "username": "Nguyen Huu Luong Anh",
        "email": "nguyenhuuluonganh@gmail.com",
        "results": null
    }
}
```

#### 7. Login
* End point: **<BASE_URL>/login**
* Method: POST
* Path params:
* Request header:
* Request body:
```
{
    "email": "nguyenthaison.nts@gmail.com",
    "password": "888888"
}
```
* Response body:
```
// SUCCESS
{
    "code": 200,
    "message": "Success",
    "data": {
        "id": 10,
        "username": "Nguyen Thai Son"
        "email": "nguyenthaison.nts@gmail.com",
        "results": null,
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAsInVzZXJuYW1lIjoiTmd1eWVuIFRoYWkgU29uIiwiZW1haWwiOiJuZ3V5ZW50aGFpc29uLm50c0BnbWFpbC5jb20iLCJyZXN1bHRzIjpudWxsLCJleHAiOjE2MjEyNjA5NDV9._-ZmeK22AYhVOpECBaz7QmMzaTz0_upgU1I-ABoX3EU",
        "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAsInVzZXJuYW1lIjoiTmd1eWVuIFRoYWkgU29uIiwiZW1haWwiOiJuZ3V5ZW50aGFpc29uLm50c0BnbWFpbC5jb20iLCJyZXN1bHRzIjpbXSwiZXhwIjoxNjIxMjYwNjQ1fQ.RIjZqnYcZI-_TPhdrl1I0CYai1qkg3OzvP8wDDKtAAo",
    }
}
```
```
// FAIL
{
    "code": 401,
    "message": "Password is incorrect",
    "data": null
}
```

#### 8. Renew Token
* End point: **<BASE_URL>/renew-token**
* Method: POST
* Path params:
* Request header:
  - **x-access-token**: Bearer <access_token>
  - **x-refresh-token**: <refresh_token>
* Request body:
* Response body:
```
// SUCCESS
{
    "code": 200,
    "message": "Success",
    "data": {
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAsInVzZXJuYW1lIjoiTmd1eWVuIFRoYWkgU29uIiwiZW1haWwiOiJuZ3V5ZW50aGFpc29uLm50c0BnbWFpbC5jb20iLCJyZXN1bHRzIjpbXSwiZXhwIjoxNjIxMjY1NDQyfQ.FLPwwAtyQzMygaLbDUMbiwCFSh0o94gJBuQYREDhEfY"
    }
}
```

```
// WRONG REFRESH TOKEN
{
    "code": 401,
    "message": "Invalid refresh token",
    "data": null
}
```
