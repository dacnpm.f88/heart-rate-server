package service

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"os"
	"strings"
	"time"
)

const EXP_TIME = 25

type authService struct {
	authRepository model.AuthRepository
}

var authSvInstance *authService

// NewAuthService creates a AuthService as a constructor.
// It received an instance of AuthRepository as an input.
func NewAuthService(r model.AuthRepository) model.AuthService {
	authSvInstance = &authService{
		r,
	}

	return authSvInstance
}

/* ======== IMPLEMENTATION ========= */

// CreateToken creates a jwt token.
// Token will be create with Claims (Payload), including {userId, username, email, result, expiry duration (in minutes)} .
func (r *authService) CreateToken (userId uint32, username, email string, results []model.Result, durationMinutes int) (string, error) {
	exp := time.Now().Add(time.Minute * time.Duration(durationMinutes)).Unix()
	claims := &model.JwtCustomClaims{
		ID:  userId,
		Username: username,
		Email: email,
		Results: results,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: exp,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(os.Getenv("API_SECRET")))
}

// GeneratePairOfTokens generates pair of AccessToken and RefreshToken from user's information.
// If everything works properly, RefreshToken will be saved to DB, and user instance will be updated AccessToken, RefreshToken field.
func (r *authService) GeneratePairOfTokens(user *model.User) (string, string, error) {
	var err error
	var accessToken string
	var refreshToken string

	accessToken, err = r.CreateToken(user.ID, user.Username, user.Email, user.Results, EXP_TIME)
	if err != nil {
		return "", "", err
	}

	refreshToken, err = r.CreateToken(user.ID, user.Username, user.Email, []model.Result{}, 0)
	if err != nil {
		return "", "", err
	}

	// save to db
	if err = r.authRepository.SaveRefreshToken(user.ID, refreshToken); err != nil {
		return "", "", err
	}

	// update user's fields
	user.RefreshToken = refreshToken
	user.AccessToken = accessToken

	return accessToken, refreshToken, err
}

// GetRefreshToken
func(r *authService) GetRefreshToken(userId uint32) (string, error) {
	return r.authRepository.FindRefreshToken(userId)
}

// ExtractToken extract the token from bearer string (Bearer <token>)
func(r *authService) ExtractToken(bearer string) (res string) {
	str := strings.Split(bearer, " ")

	if len(str) == 2 {
		res = str[1]
		return
	}

	return
}

// VerifyJwtToken checks if accessToken is valid or not.
func(r *authService) VerifyJwtToken(accessToken string) (bool, model.JwtCustomClaims, error) {
	// Initialize a new instance of `Claims`
	claims := &model.JwtCustomClaims{}

	// Parse the JWT string and store the result in `claims`.
	tkn, err := jwt.ParseWithClaims(accessToken, claims, func(token *jwt.Token) (interface{}, error) {
		secretKey := os.Getenv("API_SECRET")
		return []byte(secretKey), nil
	})

	if err != nil {
		return false, model.JwtCustomClaims{}, err
	}

	if !tkn.Valid {
		return false, model.JwtCustomClaims{}, err
	}

	return true, *claims, nil
}

// ValidateRefreshToken
func(r *authService) ValidateRefreshToken(userId uint32, refreshToken string) (bool, error) {
	var err error

	userRefreshToken, err := r.GetRefreshToken(userId)
	if err != nil {
		return false, err
	}

	if refreshToken == userRefreshToken {
		return true, nil
	}

	return false, nil
}

//func(r *authService) RenewToken(refreshToken string) (string, error) {
//	// if token is expired --> renew token
//	//v, _ := err.(*jwt.ValidationError)
//	//if v.Errors == jwt.ValidationErrorExpired && claims.ExpiresAt < time.Now().Unix() {
//	//	// refresh logic
//	//
//	//}
//
//	userRefreshToken, err := authService.GetRefreshToken(claims.ID)
//	if err != nil {
//		return controller.ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
//	}
//
//	// if refreshToken is valid --> create new accessToken
//	if refreshToken == userRefreshToken {
//		newToken, err := authService.CreateToken(claims.ID, claims.Username, claims.Email, claims.Results, service.EXP_TIME)
//		if err != nil {
//			log.Println(err)
//			return controller.ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
//		}
//
//		c.Set("newToken", newToken)
//		next(c)
//	}
//}