package service

import (
	"errors"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"log"
	"time"
)

type resultService struct {
	resultRepository model.ResultRepository
}

var resultSvInstance *resultService

// NewResultService creates a new instance of userService
func NewResultService(r model.ResultRepository) model.ResultService {
	resultSvInstance = &resultService{
		r,
	}

	return resultSvInstance
}

/* ======== IMPLEMENTATION ========= */

// Prepare standardizes input data before save to DB (trim space, ...)
func(s *resultService) Prepare(result *model.Result) {
	if result.CreatedAt.IsZero() {
		result.CreatedAt = time.Now()
	}
}

// SaveResult saves a result into DB (table: "results")
func(s *resultService) SaveResult(result *model.Result) (*model.Result, error) {
	var err error

	s.Prepare(result)

	ok, err := validate(result)
	if !ok {
		return &model.Result{}, err
	}

	result, err = s.resultRepository.Save(result)
	if err != nil {
		log.Println(err)
		return &model.Result{}, err
	}

	return result, nil
}

// FindResultByUserId selects all users' result from DB and load data to an array of model.Result
func(s *resultService) FindResultByUserId(userId uint32) (*[]model.Result, error) {
	var err error
	results := &[]model.Result{}

	results, err = s.resultRepository.FindByUserId(userId)
	if err != nil {
		return &[]model.Result{}, err
	}

	return results, err
}

func validate(result *model.Result) (bool, error) {
	if result.UserID <= 0 {
		return false, errors.New("invalid UserID")
	}
	if result.Value < 0 {
		return false, errors.New("invalid Value")
	}

	return true, nil
}