package service

import (
	"encoding/json"
	"errors"
	"github.com/badoux/checkmail"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"golang.org/x/crypto/bcrypt"
	"html"
	"strings"
	"time"
)

type userService struct {
	userRepository model.UserRepository
}

var userSvInstance *userService

// NewUserService create a UserService as a constructor.
// It received an instance of UserRepository as an input.
func NewUserService(r model.UserRepository) model.UserService {
	userSvInstance = &userService{
		r,
	}

	return userSvInstance
}

/* ======== IMPLEMENTATION ========= */

// Validate input data
func (s *userService) Validate(user *model.User, action string) error {
	switch strings.ToLower(action) {
	case "update":
		if user.Username == "" {
			return errors.New("required username")
		}
		if user.Password == "" {
			return errors.New("required password")
		}
		if user.Email == "" {
			return errors.New("required email")
		}
		if err := checkmail.ValidateFormat(user.Email); err != nil {
			return errors.New("invalid email")
		}

		return nil
	case "login":
		if user.Password == "" {
			return errors.New("required password")
		}
		if user.Email == "" {
			return errors.New("required email")
		}
		if err := checkmail.ValidateFormat(user.Email); err != nil {
			return errors.New("invalid email")
		}
		return nil

	default:
		if user.Username == "" {
			return errors.New("required username")
		}
		if user.Password == "" {
			return errors.New("required password")
		}
		if user.Email == "" {
			return errors.New("required email")
		}
		if err := checkmail.ValidateFormat(user.Email); err != nil {
			return errors.New("invalid email")
		}
		return nil
	}
}

// IsUsedEmail checks if an email has been used to register an account before
func (s *userService) IsUsedEmail(email string) (bool, error) {
	var err error
	var isUsed bool

	isUsed, err = s.userRepository.IsExisted(email)

	return isUsed, err
}

// Hash password
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// VerifyPassword checks if password is correct or not
func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// BeforeSave hashes password and reassign user.Password before save to DB
func (s *userService) BeforeSave(user *model.User) error {
	hashedPassword, err := Hash(user.Password)
	if err != nil {
		return err
	}

	user.Password = string(hashedPassword)
	return nil
}

// Prepare standardizes input data before save to DB
func (s *userService) Prepare(user *model.User) {
	user.Email = html.EscapeString(strings.TrimSpace(user.Email))
	user.Password = strings.TrimSpace(user.Password)
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
}

// SaveUser saves user to DB
func (s *userService) SaveUser(user *model.User) (*model.User, error) {
	var err error

	// inject UserRepository's method to deal with DB
	user, err = s.userRepository.Save(user)
	if err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// FindAllUsers get all users from db
func (s *userService) FindAllUsers() (*[]model.User, error) {
	var err error
	users := &[]model.User{}

	users, err = s.userRepository.FindAll()
	if err != nil {
		return &[]model.User{}, err
	}

	return users, err
}

// FindUserById get user where user's ID is id
func (s *userService) FindUserById(id uint32) (*model.User, error) {
	var err error
	user := &model.User{}

	user, err = s.userRepository.FindById(id)
	if err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// UpdateUser update user.
// It uses instance of *model.User as the new data to update for user where user's ID is id.
func (s *userService) UpdateUser(user *model.User, id uint32) (*model.User, error) {
	var err error

	user, err = s.userRepository.Update(user, id)
	if err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// DeleteUser remove user by ID
func (s *userService) DeleteUser(id uint32) (int64, error) {
	var err error
	var rowsEffected int64

	rowsEffected, err = s.userRepository.Delete(id)
	if err != nil {
		return 0, err
	}

	return rowsEffected, nil
}

// VerifyPassword verifies if password sent by user matches with hashed password from db
func (s *userService) VerifyPassword(hashedPassword string, password string) (bool, error) {
	var err error

	if err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)); err != nil {
		return false, err
	}

	return true, err
}

// FindUserByEmail gets user from DB by user's email
func(s *userService) FindUserByEmail(email string) (*model.User, error) {
	var err error
	user := &model.User{}

	user, err = s.userRepository.FindByEmail(email)
	if err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// BeforeResponse deletes some user's field before response to client
func(s *userService) BeforeResponse(user *model.User) {
	user.Password = ""
}

// RemoveUnusedFields removes 'password', 'created_at', 'updated_at', 'refresh_token' fields
// and return a map data of user.
func(s *userService) RemoveUnusedFields(user *model.User) (map[string]interface{}, error) {
	jsonData, err := json.Marshal(user)
	if err != nil {
		return map[string]interface{}{}, err
	}
	var mapData map[string]interface{}
	json.Unmarshal(jsonData, &mapData)

	delete(mapData, "created_at")
	delete(mapData, "updated_at")
	delete(mapData, "refresh_token")
	delete(mapData, "password")

	return mapData, nil
}