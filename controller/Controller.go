package controller

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gitlab.com/dacnpm.f88/heart-rate-server/repository"
	"gitlab.com/dacnpm.f88/heart-rate-server/service"
	"gitlab.com/dacnpm.f88/heart-rate-server/utils"
	"net/http"
)

var (
	userRepository  model.UserRepository
	userService model.UserService
	authRepository model.AuthRepository
	authService model.AuthService
	resultRepository model.ResultRepository
	resultService model.ResultService
)

func init() {
	userRepository = repository.NewUserRepository()
	userService = service.NewUserService(userRepository)
	authRepository = repository.NewAuthRepository()
	authService = service.NewAuthService(authRepository)
	resultRepository = repository.NewResultRepository()
	resultService = service.NewResultService(resultRepository)
}

func ApiResult(c echo.Context, status int, message string, data interface{}) error {
	response := &utils.Reponse{
		Code: status,
		Message: message,
		Data: data,
	}

	return c.JSON(status, response)
}

func Home(c echo.Context) error {
	return ApiResult(c, http.StatusOK, "Success", "Get all users")
}
