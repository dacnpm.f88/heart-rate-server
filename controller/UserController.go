package controller

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gitlab.com/dacnpm.f88/heart-rate-server/service"
	"log"
	"net/http"
	"strconv"
)

// GetAllUsers gets all users from DB
func GetAllUsers(c echo.Context) error {
	var err error
	users := &[]model.User{}

	users, err = userService.FindAllUsers()
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Something has been broken", []model.User{})
	}

	return ApiResult(c, http.StatusOK, "Success", users)
}

// GetUserById gets user by user id
func GetUserById(c echo.Context) error {
	var err error
	user := &model.User{}

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Error when parse data to uint64", model.User{})
	}

	user, err = userService.FindUserById(uint32(id))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Error when find user by id", model.User{})
	}

	return ApiResult(c, http.StatusOK, "Success", user)
}

//Register creates a new user
func Register(c echo.Context) error {
	var err error
	user := &model.User{}

	// bind request data to user struct
	if err = c.Bind(user); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusOK, "Failed to bind data from request", model.User{})
	}

	// validate request data (validate email & empty fields)
	if err = userService.Validate(user, "create"); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, err.Error(), nil)
	}

	// check if email was used to register before
	if isUsed, _ := userService.IsUsedEmail(user.Email); isUsed {
		log.Println("Email was used")
		return ApiResult(c, http.StatusBadRequest, "Email was used by another account", nil)
	}

	userService.Prepare(user)    // trim email
	userService.BeforeSave(user) // hash password

	// save to db
	user, err = userService.SaveUser(user)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Failed to create user", nil)
	}

	responseData, err := userService.RemoveUnusedFields(user)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
	}

	return ApiResult(c, http.StatusOK, "Success", responseData)
}

func Login(c echo.Context) error {
	var err error
	var user model.User

	// binding request data to User struct
	if err = c.Bind(&user); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to parse request data", nil)
	}

	// validating request data
	if err = userService.Validate(&user, "login"); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, err.Error(), nil)
	}

	userService.Prepare(&user) // trimming & html escaping data

	// get user from db by email
	userFromDB, err := userService.FindUserByEmail(user.Email)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
	}

	// verify password
	if correct, err := userService.VerifyPassword(userFromDB.Password, user.Password); !correct {
		log.Println("Password is incorrect")
		log.Println("error: ", err)
		return ApiResult(c, http.StatusUnauthorized, "Password is incorrect", nil)
	}

	// generate tokens
	accessToken, refreshToken, err := authService.GeneratePairOfTokens(userFromDB)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
	}

	log.Println("accessTk: ", accessToken)
	log.Println("refreshTk: ", refreshToken)
	userService.BeforeResponse(userFromDB)

	jsonData, _ := json.Marshal(userFromDB)
	var mapData map[string]interface{}
	json.Unmarshal(jsonData, &mapData)

	mapData["access_token"] = accessToken
	mapData["refresh_token"] = refreshToken
	delete(mapData, "password")

	return ApiResult(c, http.StatusOK, "Success", mapData)
}

func RenewToken(c echo.Context) error {
	// get user's ID
	userId, err := strconv.Atoi(c.Param("user-id"))
	if err != nil {
		return ApiResult(c, http.StatusBadRequest, "Failed to parse request data", nil)
	}

	// get access token from request header
	bearerStr := c.Request().Header.Get("x-access-token")
	accessToken := authService.ExtractToken(bearerStr)

	// get refresh token from request header
	refreshToken := c.Request().Header.Get("x-refresh-token")

	_, _ , err = authService.VerifyJwtToken(accessToken)
	if err != nil {
		// if token is expired --> renew token
		v, _ := err.(*jwt.ValidationError)
		if v.Errors == jwt.ValidationErrorExpired {
			ok, err := authService.ValidateRefreshToken(uint32(userId), refreshToken)
			if err != nil {
				log.Println(err)
				return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
			}

			// if refreshToken is invalid
			if !ok {
				return ApiResult(c, http.StatusUnauthorized, "Invalid refresh token", nil)
			}

			// get user
			user, err := userService.FindUserById(uint32(userId))
			if err != nil {
				log.Println(err)
				return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
			}

			// create new access token
			newAccessToken, err := authService.CreateToken(user.ID, user.Username, user.Email, user.Results, service.EXP_TIME)
			if err != nil {
				log.Println(err)
				return ApiResult(c, http.StatusInternalServerError, "Something went wrong", nil)
			}

			return ApiResult(c, http.StatusOK, "Success", map[string]interface{}{"access_token": newAccessToken})
		}
	}

	return ApiResult(c, http.StatusBadRequest, "", nil)
}

func UpdateUser(c echo.Context) error {
	var err error
	user := &model.User{}

	// get user's id from path parameter
	uid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to get user's id from path parameter", model.User{})
	}

	// bind request data
	if err = c.Bind(&user); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to bind data from request", model.User{})
	}

	// validate request data (validate email & empty fields)
	if err = userService.Validate(user, "update"); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, err.Error(), nil)
	}

	userService.Prepare(user)
	userService.BeforeSave(user) // hash password

	// update DB
	user, err = userService.UpdateUser(user, uint32(uid))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Failed to update DB", model.User{})
	}

	responseData, _ := userService.RemoveUnusedFields(user)

	return ApiResult(c, http.StatusOK, "Success", responseData)
}

func DeleteUser(c echo.Context) error {
	var err error

	uid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to parse parameter", nil)
	}

	// update DB
	_ , err = userService.DeleteUser(uint32(uid))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Failed", nil)
	}

	return ApiResult(c, http.StatusOK, "Success", nil)
}
