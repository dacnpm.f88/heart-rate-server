package controller

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"log"
	"net/http"
	"strconv"
)

func AddUserResult(c echo.Context) error {
	var err error
	var uid int
	var result model.Result

	uid, err = strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to parse parameters", nil)
	}

	if err = c.Bind(&result); err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to parse parameters", nil)
	}

	result.UserID = uint32(uid)

	rs, err := resultService.SaveResult(&result)
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return ApiResult(c, http.StatusOK, "Success", rs)
}

func GetAllUserResults(c echo.Context) error {
	var err error
	var uid int
	var results *[]model.Result

	uid, err = strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusBadRequest, "Failed to parse parameters", nil)
	}

	results, err = resultService.FindResultByUserId(uint32(uid))
	if err != nil {
		log.Println(err)
		return ApiResult(c, http.StatusInternalServerError, "Failed", nil)
	}

	return ApiResult(c, http.StatusOK, "Success", results)
}
