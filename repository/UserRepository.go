package repository

import (
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gorm.io/gorm"
)

// This struct implements all the methods declared in model.UserRepository Interface
type userRepository struct {
	DB *gorm.DB
}

// UserRepository's contructor
func NewUserRepository() model.UserRepository {
	db := getGormDbInstance()
	return &userRepository {
		db,
	}
}

// IsExisted checks if an email has been used to register an account before
func (u *userRepository) IsExisted(email string) (bool, error) {
	var err error
	var count int64 = 0
	var user model.User

	err = u.DB.Debug().Where(&model.User{Email: email}).Find(&user).Count(&count).Error
	if err != nil {
		return false, err
	}

	if count < 1 {
		return false, err
	}

	return true, err
}

// Save user record to DB
func (u *userRepository) Save(user *model.User) (*model.User, error) {
	var err error

	if err = u.DB.Debug().Create(&user).Error; err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// Find all users' records
func (u *userRepository) FindAll() (*[]model.User, error) {
	var err error
	users := []model.User{}

	err = u.DB.Debug().Model(&model.User{}).Find(&users).Error
	if err != nil {
		return &[]model.User{}, err
	}

	return &users, nil
}

// Find user by id
func (u *userRepository) FindById(id uint32) (*model.User, error) {
	var err error
	user := model.User{}

	err = u.DB.Debug().Preload("Results").First(&user, id).Error
	if err != nil {
		return &model.User{}, err
	}

	return &user, nil
}

// Update user
func (u *userRepository) Update(user *model.User, id uint32) (*model.User, error) {
	var err error

	// update fields
	db := u.DB.Debug().Model(&model.User{}).Where("id = ?", id).Take(&model.User{}).UpdateColumns(model.User{
		Username: user.Username,
		Email: user.Email,
		Password: user.Password,
	})

	// check error
	if err = db.Error; err != nil {
		return &model.User{}, err
	}

	// This is the display the updated user
	err = db.Debug().Model(&model.User{}).Where("id = ?", id).Take(user).Error
	if err != nil {
		return &model.User{}, err
	}

	return user, nil
}

// Delete user
func (u *userRepository) Delete(id uint32) (int64, error) {
	db := u.DB.Debug().Model(&model.User{}).Where("id = ?", id).Take(&model.User{}).Delete(&model.User{})

	if db.Error != nil {
		return 0, db.Error
	}
	return u.DB.RowsAffected, nil
}

// FindByEmail selects a record of user from "users" table where user's email equals to 'email' param
func (u *userRepository) FindByEmail(email string) (*model.User, error) {
	var err error
	user := model.User{}

	err = u.DB.Debug().Where(&model.User{Email: email}).Find(&user).Error
	if err != nil {
		return &model.User{}, err
	}

	return &user, nil
}