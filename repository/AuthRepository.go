package repository

import (
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gorm.io/gorm"
)

type authRepository struct {
	DB *gorm.DB
}

func NewAuthRepository() model.AuthRepository {
	db := getGormDbInstance()
	return &authRepository {
		db,
	}
}

func(r *authRepository) SaveRefreshToken(userId uint32, refreshToken string) error {
	var err error

	if err = r.DB.Debug().Where(&model.User{ID: userId}).Updates(&model.User{RefreshToken: refreshToken}).Error; err != nil {
		return err
	}

	return nil
}

func(r *authRepository) FindRefreshToken(userId uint32) (string, error) {
	var err error
	var user model.User

	if err = r.DB.Debug().Where(&model.User{ID: userId}).Find(&user).Error; err != nil {
		return "", err
	}

	return user.RefreshToken, nil
}


