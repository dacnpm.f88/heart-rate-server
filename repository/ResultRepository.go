package repository

import (
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gorm.io/gorm"
	"log"
)

// This struct implements all the methods declared in model.UserRepository Interface
type resultRepository struct {
	DB *gorm.DB
}

// NewResultRepository creates a new instance of resultRepository
func NewResultRepository() model.ResultRepository {
	db := getGormDbInstance()
	return &resultRepository {
		db,
	}
}

// Save inserts a record of result into "results" table
// Returns a reference to a model.Result and an error
func(rs *resultRepository) Save(result *model.Result) (*model.Result, error) {
	var err error

	if err = rs.DB.Debug().Create(&result).Error; err != nil {
		return &model.Result{}, err
	}

	return result, nil
}

// FindByUserId selects all records of results belong to user whose id is userId
// Returns an array of model.Result and an error
func(rs *resultRepository) FindByUserId(userId uint32) (*[]model.Result, error) {
	var err error
	var results []model.Result

	err = rs.DB.Debug().Model(&model.Result{}).Where(&model.Result{UserID: userId}).Find(&results).Error
	if err != nil {
		log.Println(err)
		return &[]model.Result{}, err
	}

	return &results, err
}
