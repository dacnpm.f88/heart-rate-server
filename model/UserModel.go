package model

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type User struct {
	ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Username  string    `gorm:"size:255;not null;unique" json:"username"`
	Email     string    `gorm:"size:100;not null;unique" json:"email"`
	Password  string    `gorm:"size:100;not null;" json:"password"`
	Results   []Result  `json:"results"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"-"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"-"`
	AccessToken string  `gorm:"-" json:"-"`
	RefreshToken string `json:"-"`
}

type JwtCustomClaims struct {
	ID       uint32         `json:"id"`
	Username string         `json:"username"`
	Email    string         `json:"email"`
	Results  []Result 		`json:"results"`
	jwt.StandardClaims
}

type UserService interface {
	// Validate checks if one field in User is null
	Validate(user *User, action string) error

	// IsUsedEmail checks if an email has been used to register an account before
	IsUsedEmail(email string) (bool, error)

	// BeforeSave hashes password and re-assign user.Password before save to DB
	BeforeSave(user *User) error

	// Prepare standardizes input data before save to DB (trim space, ...)
	Prepare(user *User)

	// SaveUser saves a user into DB (table: "users")
	SaveUser(user *User) (*User, error)

	// FindAllUsers selects all users from DB and load data to an array of model.User
	FindAllUsers() (*[]User, error)

	// FindUserById gets user from DB by user's id
	FindUserById(id uint32) (*User, error)

	// UpdateUser updates an user to DB by user's id
	UpdateUser(user *User, id uint32) (*User, error)

	// DeleteUser deletes user by user's id
	DeleteUser(id uint32) (int64, error)

	// VerifyPassword verifies if password sent by user matches with hashed password from db
	VerifyPassword(hashedPassword string, password string) (bool, error)

	// FindUserByEmail gets user from DB by user's email
	FindUserByEmail(email string) (*User, error)

	// BeforeResponse deletes some user's field before response to client
	BeforeResponse(user *User)

	// RemoveUnusedFields removes 'password', 'created_at', 'updated_at', 'refresh_token' fields
	// and return a map data of user.
	RemoveUnusedFields(user *User) (map[string]interface{}, error)
}

// UserRepository defines methods working with db
type UserRepository interface {
	// IsExisted checks if an email has been used to register an account before
	IsExisted(email string) (bool, error)

	// Save inserts a record of user into "users" table
	Save(user *User) (*User, error)

	// FindById selects a record of user from "users" table where user's id equals to 'id' param
	FindById(id uint32) (*User, error)

	// FindAll selects all records from "users" table and load them to an array of model.User
	FindAll() (*[]User, error)

	// Update updates a record of user where id equals to 'id' param
	Update(user *User, id uint32) (*User, error)

	// Delete deletes a record from "users" table where id equals to 'id'.
	// Returns db rows effected and an error
	Delete(id uint32) (int64, error)

	// FindByEmail selects a record of user from "users" table where user's email equals to 'email' param
	FindByEmail(email string) (*User, error)
}

// AuthService defines method used to authenticate and protect routes
type AuthService interface {
	// CreateToken creates a jwt token.
	// Token will be create with Claims (Payload), including {userId, username, email, result, expiry duration (in minutes)} .
	CreateToken(userId uint32, username, email string, results []Result, durationMinutes int) (string, error)

	// GeneratePairOfTokens generates pair of AccessToken and RefreshToken from user's information.
	// If everything works properly, RefreshToken will be saved to DB.
	GeneratePairOfTokens(user *User) (accessToken string, refreshToken string, err error)

	// GetRefreshToken gets the refreshToken of user from DB
	GetRefreshToken(userId uint32) (string, error)

	// ExtractToken extract the token from bearer string (Bearer <token>)
	ExtractToken(bearer string) (string)

	// VerifyJwtToken checks if accessToken is valid or not
	VerifyJwtToken(accessToken string) (bool, JwtCustomClaims, error)

	// ValidateRefreshToken checks if user's refreshToken is valid or not
	ValidateRefreshToken(userId uint32, refreshToken string) (bool, error)
}

type AuthRepository interface {
	// SaveRefreshToken saves (or updates) Refresh Token of user in DB
	SaveRefreshToken(userId uint32, refreshToken string) error

	// FindRefreshToken selects Refresh Token of user from DB
	FindRefreshToken(userId uint32) (string, error)
}
