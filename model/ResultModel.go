package model

import (
	"time"
)

type Result struct {
	ID        uint32    `gorm:"primary_key;not null; auto_increment" json:"id"`
	UserID    uint32    `gorm:"not null" json:"user_id"` // Implicit foreign key
	Value     int       `gorm:"not null" json:"value"`
	Duration  int       `gorm:"default:60" json:"duration"`
	Note      string    `gorm:"size:200" json:"note"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
}

type ResultService interface {
	// Prepare standardizes input data before save to DB (trim space, ...)
	Prepare(result *Result)

	// SaveResult saves a result into DB (table: "results")
	SaveResult(result *Result) (*Result, error)

	// FindResultByUserId selects all users' result from DB and load data to an array of model.Result
	FindResultByUserId(userId uint32) (*[]Result, error)
}

type ResultRepository interface {
	// Save inserts a record of result into "results" table
	// Returns a reference to a model.Result and an error
	Save(result *Result) (*Result, error)

	// FindByUserId selects all records of results belong to user whose id is userId
	// Returns an array of model.Result and an error
	FindByUserId(userId uint32) (*[]Result, error)
}
