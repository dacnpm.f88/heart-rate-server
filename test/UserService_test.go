package test

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gitlab.com/dacnpm.f88/heart-rate-server/service"
	"testing"
	"time"
)

// Create MOCK Dependency
type UserRepositoryMock struct {
	mock.Mock
}

func(m *UserRepositoryMock) Save(user *model.User) (*model.User, error) {
	args := m.Called(user)
	return args.Get(0).(*model.User), args.Error(1)
}

func(m *UserRepositoryMock) FindById(id uint32) (*model.User, error) {
	args := m.Called(id)
	return args.Get(0).(*model.User), args.Error(1)
}

func(m *UserRepositoryMock) FindAll() (*[]model.User, error) {
	args := m.Called()
	return args.Get(0).(*[]model.User), args.Error(1)
}

func(m *UserRepositoryMock) Update(user *model.User, id uint32) (*model.User, error) {
	args := m.Called(user, id)
	return args.Get(0).(*model.User), args.Error(1)
}

func(m *UserRepositoryMock) Delete(id uint32) (int64, error) {
	args := m.Called(id)
	return args.Get(0).(int64), args.Error(1)
}

func(m *UserRepositoryMock) FindByEmail(email string) (*model.User, error) {
	args := m.Called(email)
	return args.Get(0).(*model.User), args.Error(1)
}

func(m *UserRepositoryMock) IsExisted(email string) (bool, error) {
	args := m.Called(email)
	return args.Get(0).(bool), args.Error(1)
}

// TestValidate tests Validate method of User
func TestValidate(t *testing.T) {
	// define input struct correspond to method's arguments
	type input struct {
		user *model.User
		action string
	}

	// define testcases slice
	cases := []struct {
		name string // name of the testcase
		want error // what result we want our function to return // model.user
		wantErr bool // whether or not we want an error
		inputArgs input // the arguments used for this test
	}{
		// declaring each unit test input and output data as defined before
		{"Full-filled User's information. Action: unset.", nil, false, input{
			user:   &model.User{
				Username:  "Pham Tan Tai",
				Email:     "phamtantai@gmail.com",
				Password:  "78910",
			},
			action: "",
		}},
		{"Username is nil. Action: unset.", errors.New("required username"), true, input{
			user:   &model.User{
				Email:     "phamtantai@gmail.com",
				Password:  "78910",
			},
			action: "",
		}},
		{"Email is nil. Action: unset.", errors.New("required email"), true, input{
			user:   &model.User{
				Username:  "Pham Tan Tai",
				Password:  "78910",
			},
			action: "",
		}},
		{"Password is nil. Action: unset.", errors.New("required password"), true, input{
			user:   &model.User{
				Username:  "Pham Tan Tai",
				Email: "phamtantai@gmail.com",
			},
			action: "",
		}},
		{"Full-filled user's information. Email is invalid. Action: unset.", errors.New("invalid email"), true, input{
			user:   &model.User{
				Username:  "Pham Tan Tai",
				Email: "phamtantai.gmail.com",
				Password: "78910",
			},
			action: "",
		}},
		{"Full-filled user's information. Action: 'update'.", nil, false, input{
			user:   &model.User{
				Username:  "Pham Tan Tai",
				Email: "phamtantai@gmail.com",
				Password: "78910",
			},
			action: "update",
		}},
		{"Input: Email & Password. Action: 'login'.", nil, false, input{
			user:   &model.User{
				Email: "phamtantai@gmail.com",
				Password: "78910",
			},
			action: "login",
		}},
	}

	userService := service.NewUserService(&UserRepositoryMock{})

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.want, userService.Validate(tc.inputArgs.user, tc.inputArgs.action))
		})
	}
}

// TestFindUserById tests UserService's FindUserById method
func TestFindUserById(t *testing.T) {
	// prepare mock dependency instance
	userRepositoryMock := UserRepositoryMock{}
	userRepositoryMock.On("FindById", uint32(1)).Return(&model.User{
		ID:        1,
		Username:  "Nguyen Thai Son",
		Email:     "nguyenthaison247@gmail.com",
		Password:  "78910",
		Results:   []model.Result{
			{
				ID:        1,
				UserID:    1,
				Value:    86,
				Duration:  60,
				Note:      "This is a note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}, nil)
	userRepositoryMock.On("FindById", uint32(2)).Return(&model.User{
		ID:        2,
		Username:  "Nguyen Huu Luong Anh",
		Email:     "nguyenhuuluonganh@gmail.com",
		Password:  "300475",
		Results:   []model.Result{
			{
				ID:        2,
				UserID:    2,
				Value:    79,
				Duration:  60,
				Note:      "This is my note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}, nil)
	// case error user does not exist
	userRepositoryMock.On("FindById", uint32(99)).Return(&model.User{}, errors.New("user not found"))


	// define input struct
	type input struct {
		uid uint32
	}

	// define output struct
	type output struct {
		user *model.User
		err error
	}

	// define testcases slice
	cases := []struct {
		name string
		want output
		wantErr bool
		inputArgs input
	}{
		{"Get an existing user", output{&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil}, false, input{1}},
		{"Get an existing user", output{&model.User{
			ID:        2,
			Username:  "Nguyen Huu Luong Anh",
			Email:     "nguyenhuuluonganh@gmail.com",
			Password:  "300475",
			Results:   []model.Result{
				{
					ID:        2,
					UserID:    2,
					Value:    79,
					Duration:  60,
					Note:      "This is my note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil}, false, input{2}},
		{"Get an user that does not exist", output{&model.User{}, errors.New("user not found")}, true, input{99}},
	}

	userService := service.NewUserService(&userRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			user, err := userService.FindUserById(tc.inputArgs.uid)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.user, user)
		})
	}
}

func TestFindAllUsers(t *testing.T) {
	// prepare mock dependency instance
	userRepositoryMock := UserRepositoryMock{}
	userRepositoryMock.On("FindAll").Return(&[]model.User{
		{
				ID:        1,
				Username:  "Nguyen Thai Son",
				Email:     "nguyenthaison247@gmail.com",
				Password:  "78910",
				Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
		},
		{
				ID:        2,
				Username:  "Nguyen Huu Luong Anh",
				Email:     "nguyenhuuluonganh@gmail.com",
				Password:  "300475",
				Results:   []model.Result{
				{
					ID:        2,
					UserID:    2,
					Value:    79,
					Duration:  60,
					Note:      "This is my note",
					CreatedAt: time.Time{},
				},
			},
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
		},
	}, nil)

	// define output struct
	type output struct {
		user *[]model.User
		err error
	}

	// define testcases slice
	cases := []struct {
		name string
		want output
		wantErr bool
	}{
		{"Get all user", output{&[]model.User{
			{
				ID:        1,
				Username:  "Nguyen Thai Son",
				Email:     "nguyenthaison247@gmail.com",
				Password:  "78910",
				Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
				},
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
		},
		{
				ID:        2,
				Username:  "Nguyen Huu Luong Anh",
				Email:     "nguyenhuuluonganh@gmail.com",
				Password:  "300475",
				Results:   []model.Result{
				{
					ID:        2,
					UserID:    2,
					Value:    79,
					Duration:  60,
					Note:      "This is my note",
					CreatedAt: time.Time{},
				},
				},
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
		}}, nil}, false},
	}

	userService := service.NewUserService(&userRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			users, err := userService.FindAllUsers()

			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.user, users)
		})
	}
}

func TestSaveUser(t *testing.T) {
	// prepare mock dependency instance
	userRepositoryMock := UserRepositoryMock{}
	// case  user is not exist
	userRepositoryMock.On("Save", &model.User{
		ID:        1,
		Username:  "Nguyen Thai Son",
		Email:     "nguyenthaison247@gmail.com",
		Password:  "78910",
		Results:   []model.Result{
			{
				ID:        1,
				UserID:    1,
				Value:    86,
				Duration:  60,
				Note:      "This is a note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}).Return(
		&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil)
	// case error user existed
	userRepositoryMock.On("Save", &model.User{
		ID:        2,
		Username:  "Pham Tan Tai",
		Email:     "tai45236@gmail.com",
		Password:  "7890",
		Results:   []model.Result{
			{
				ID:        1,
				UserID:    1,
				Value:    86,
				Duration:  60,
				Note:      "This is a note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}).Return(&model.User{}, errors.New("user existed"))

	// define input struct
	type input struct {
		user *model.User
	}

	// define output struct
	type output struct {
		user *model.User
		err error
	}

	// define testcases slice
	cases := []struct {
		name string
		want output
		wantErr bool
		inputArgs input
	}{
		{"Save a new user", output{&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil}, false, input{&model.User{
				ID:        1,
				Username:  "Nguyen Thai Son",
				Email:     "nguyenthaison247@gmail.com",
				Password:  "78910",
				Results:   []model.Result{
					{
						ID:        1,
						UserID:    1,
						Value:    86,
						Duration:  60,
						Note:      "This is a note",
						CreatedAt: time.Time{},
					},
				},
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		}},
		{"Save a existed user", output{&model.User{}, errors.New("user existed")}, true, input{ &model.User{
			ID:        2,
			Username:  "Pham Tan Tai",
			Email:     "tai45236@gmail.com",
			Password:  "7890",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		},
		}},
	}

	userService := service.NewUserService(&userRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			user, err := userService.SaveUser(tc.inputArgs.user)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.user, user)
		})
	}
}

func TestUpdateUser(t *testing.T) {
	// prepare mock dependency instance
	userRepositoryMock := UserRepositoryMock{}
	// case  user is not exist
	userRepositoryMock.On("Update", &model.User{
		ID:        1,
		Username:  "Nguyen Thai Son",
		Email:     "nguyenthaison247@gmail.com",
		Password:  "78910",
		Results:   []model.Result{
			{
				ID:        1,
				UserID:    1,
				Value:    86,
				Duration:  60,
				Note:      "This is a note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}, uint32(1)).Return(
		&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil)
	// case error user existed
	userRepositoryMock.On("Update", &model.User{
		ID:        2,
		Username:  "Pham Tan Tai",
		Email:     "tai45236@gmail.com",
		Password:  "7890",
		Results:   []model.Result{
			{
				ID:        1,
				UserID:    1,
				Value:    86,
				Duration:  60,
				Note:      "This is a note",
				CreatedAt: time.Time{},
			},
		},
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}, uint32(2)).Return(&model.User{}, errors.New("user not found"))

	// define input struct
	type input struct {
		user *model.User
		id uint32
	}

	// define output struct
	type output struct {
		user *model.User
		err error
	}

	// define testcases slice
	cases := []struct {
		name string
		want output
		wantErr bool
		inputArgs input
	}{
		{"Save a new user", output{&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, nil}, false, input{&model.User{
			ID:        1,
			Username:  "Nguyen Thai Son",
			Email:     "nguyenthaison247@gmail.com",
			Password:  "78910",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}, 1,
		}},
		{"Save a existed user", output{&model.User{}, errors.New("user not found")}, true, input{ &model.User{
			ID:        2,
			Username:  "Pham Tan Tai",
			Email:     "tai45236@gmail.com",
			Password:  "7890",
			Results:   []model.Result{
				{
					ID:        1,
					UserID:    1,
					Value:    86,
					Duration:  60,
					Note:      "This is a note",
					CreatedAt: time.Time{},
				},
			},
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		},2,
		}},
	}

	userService := service.NewUserService(&userRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			user, err := userService.UpdateUser(tc.inputArgs.user, tc.inputArgs.id)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.user, user)
		})
	}
}

func TestDeleteUser(t *testing.T) {
	// prepare mock dependency instance
	userRepositoryMock := UserRepositoryMock{}
	userRepositoryMock.On("Delete", uint32(1)).Return(int64(1), nil)
	// case error user does not exist
	userRepositoryMock.On("Delete", uint32(2)).Return(int64(0), errors.New("user not found"))

	// define input struct
	type input struct {
		uid uint32
	}

	// define output struct
	type output struct {
		rowsEffected int64
		err error
	}

	// define testcases slice
	cases := []struct {
		name string
		want output
		wantErr bool
		inputArgs input
	}{
		{"Get an existing user", output{int64(1), nil}, false, input{1}},
		{"Get an user that does not exist", output{0, errors.New("user not found")}, true, input{2}},
	}

	userService := service.NewUserService(&userRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			rowsEffected, err := userService.DeleteUser(tc.inputArgs.uid)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.rowsEffected, rowsEffected)
		})
	}
}
