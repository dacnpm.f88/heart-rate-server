package test

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gitlab.com/dacnpm.f88/heart-rate-server/service"
	"testing"
	"time"
)

// Create MOCK Dependency
type ResultRepositoryMock struct {
	mock.Mock
}

func (m *ResultRepositoryMock) Save(result *model.Result) (*model.Result, error) {
	args := m.Called(result)
	return args.Get(0).(*model.Result), args.Error(1)
}

func (m *ResultRepositoryMock) FindByUserId(userId uint32) (*[]model.Result, error) {
	args := m.Called(userId)
	return args.Get(0).(*[]model.Result), args.Error(1)
}

func TestSaveResult(t *testing.T) {
	now := time.Now()
	// prepare mock dependency instance
	resultRepositoryMock := ResultRepositoryMock{}
	resultRepositoryMock.On("Save", &model.Result{
		UserID:   2,
		Value:    79,
		Duration: 60,
		Note:     "Some text here",
		CreatedAt: now,
	}).Return(&model.Result{
		ID:        1,
		UserID:    2,
		Value:     79,
		Duration:  60,
		Note:      "Some text here",
		CreatedAt: now,
	}, nil)
	resultRepositoryMock.On("Save", &model.Result{
		UserID:   99,
		Value:    79,
		Duration: 60,
		Note:     "Some text here",
		CreatedAt: now,
	}).Return(&model.Result{}, errors.New("ERROR: insert or update on table \"results\" violates foreign key constraint \"fk_users_results\" (SQLSTATE 23503)"))

	// define a struct containing fields corresponding to method's arguments
	type input struct {
		result *model.Result
	}

	// define a struct containing fields corresponding to method's returned values
	type output struct {
		result *model.Result
		err    error
	}

	// define testcases slice
	cases := []struct {
		name      string
		want      output
		wantErr   bool
		inputArgs input
	}{
		{
			"Valid input data",
			output{
				&model.Result{
						ID:        1,
						UserID:    2,
						Value:     79,
						Duration:  60,
						Note:      "Some text here",
						CreatedAt: now,
					},
				nil},
			false,
			input{
				&model.Result{
					UserID:   2,
					Value:    79,
					Duration: 60,
					Note:     "Some text here",
					CreatedAt: now,
				},
			},
		},
		{
			"Invalid input data: negative value",
			output{
			&model.Result{},
			errors.New("invalid Value")},
			false,
			input{
				&model.Result{
					UserID:   2,
					Value:    -79,
					Duration: 60,
					Note:     "Some text here",
					CreatedAt: now,
				},
			},
		},
		{
			"Invalid input data: user does not exist",
			output{
				&model.Result{},
				errors.New("ERROR: insert or update on table \"results\" violates foreign key constraint \"fk_users_results\" (SQLSTATE 23503)"),
			},
			false,
			input{
				&model.Result{
					UserID:   99,
					Value:    79,
					Duration: 60,
					Note:     "Some text here",
					CreatedAt: now,
				},
			},
		},
	}

	resultService := service.NewResultService(&resultRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			result, err := resultService.SaveResult(tc.inputArgs.result)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.result, result)
		})
	}
}

func TestFindResultByUserId(t *testing.T) {
	now := time.Now()
	// prepare mock dependency instance
	resultRepositoryMock := ResultRepositoryMock{}
	resultRepositoryMock.On("FindByUserId", uint32(2)).Return(&[]model.Result{
		{
			ID:        1,
			UserID:    2,
			Value:     79,
			Duration:  60,
			Note:      "Some text here 01",
			CreatedAt: now,
		},
		{
			ID:        1,
			UserID:    2,
			Value:     89,
			Duration:  60,
			Note:      "Some text here 02",
			CreatedAt: now,
		},
	}, nil)
	resultRepositoryMock.On("FindByUserId", uint32(3)).Return(&[]model.Result{}, nil)
	resultRepositoryMock.On("FindByUserId", uint32(99)).Return(&[]model.Result{}, errors.New("ERROR: insert or update on table \"results\" violates foreign key constraint \"fk_users_results\" (SQLSTATE 23503)"))

	// define a struct containing fields corresponding to method's arguments
	type input struct {
		uid uint32
	}

	// define a struct containing fields corresponding to method's returned values
	type output struct {
		results *[]model.Result
		err     error
	}

	// define testcases slice
	cases := []struct {
		name      string
		want      output
		wantErr   bool
		inputArgs input
	}{
		{
			"Valid input data",
			output{
				&[]model.Result{
					{
						ID:        1,
						UserID:    2,
						Value:     79,
						Duration:  60,
						Note:      "Some text here 01",
						CreatedAt: now,
					},
					{
						ID:        1,
						UserID:    2,
						Value:     89,
						Duration:  60,
						Note:      "Some text here 02",
						CreatedAt: now,
					},
				},
				nil},
			false,
			input{2},
		},
		{
			"User does not have any result",
			output{&[]model.Result{}, nil},
			false,
			input{3},
		},
		{
			"User does not exist",
			output{&[]model.Result{}, errors.New("ERROR: insert or update on table \"results\" violates foreign key constraint \"fk_users_results\" (SQLSTATE 23503)")},
			true,
			input{99},
		},
	}

	resultService := service.NewResultService(&resultRepositoryMock)

	// iterating over the previous testcases slice
	for _, tc := range cases { // for each, tc = cases[0]
		t.Run(tc.name, func(t *testing.T) {
			results, err := resultService.FindResultByUserId(tc.inputArgs.uid)
			if tc.wantErr {
				assert.Equal(t, tc.want.err, err)
			}
			assert.Equal(t, tc.want.results, results)
		})
	}
}
