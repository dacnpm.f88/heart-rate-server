package auth
//
//import (
//	"github.com/dgrijalva/jwt-go"
//	"gitlab.com/dacnpm.f88/heart-rate-server/model"
//	"log"
//	"os"
//	"time"
//)
//
//const EXP_TIME = 5
//
//type CustomClaims struct {
//	ID       uint32         `json:"id"`
//	Username string         `json:"username"`
//	Email    string         `json:"email"`
//	Results  []model.Result `json:"results"`
//	jwt.StandardClaims
//}
//
//type UserToken struct {
//	ID        uint32    `gorm:"primary_key" json:"id"`
//	RefreshToken string `gorm:"not null;unique" json:"refresh_token"`
//}
//
//func CreateToken(userId uint32, username, email string, results []model.Result, durationMinutes int) (string, error) {
//	exp := time.Now().Add(time.Minute * time.Duration(durationMinutes)).Unix()
//	claims := &CustomClaims{
//		ID:  userId,
//		Username: username,
//		Email: email,
//		Results: results,
//		StandardClaims: jwt.StandardClaims{
//			ExpiresAt: exp,
//		},
//	}
//	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
//
//	return token.SignedString([]byte(os.Getenv("API_SECRET")))
//}
//
//func GeneratePairOfTokens(user model.User) (accessToken string, refreshToken string, err error) {
//	accessToken, err = CreateToken(user.ID, user.Username, user.Email, user.Results, EXP_TIME)
//	if err != nil {
//		log.Println(err)
//	}
//
//	refreshToken, err = CreateToken(user.ID, user.Username, user.Email, []model.Result{}, 0)
//	if err != nil {
//		log.Println(err)
//	}
//
//	return accessToken, refreshToken, err
//}
