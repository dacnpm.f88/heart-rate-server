package mdw

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/dacnpm.f88/heart-rate-server/controller"
	"log"
	"net/http"
	"strconv"
)

func ValidateAuthentication(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// get access token from request header
		bearerStr := c.Request().Header.Get("x-access-token")
		accessToken := authService.ExtractToken(bearerStr)

		// verify access token
		_, _ , err := authService.VerifyJwtToken(accessToken)
		if err != nil {
			log.Println(err)
			// if token is expired --> renew token
			v, _ := err.(*jwt.ValidationError)
			if v.Errors == jwt.ValidationErrorExpired {
				return controller.ApiResult(c, http.StatusUnauthorized, "Token is expired", nil)
			}

			return controller.ApiResult(c, http.StatusUnauthorized, "Invalid token", nil)
		}

		return next(c)
	}
}

func CheckUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// get access token from request header
		bearerStr := c.Request().Header.Get("x-access-token")
		accessToken := authService.ExtractToken(bearerStr)

		// get user id from url param
		paramUserId, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return controller.ApiResult(c, http.StatusBadRequest, "Failed to parse request data", nil)
		}

		// verify access token
		_, claims , err := authService.VerifyJwtToken(accessToken)
		if err != nil {
			log.Println(err)
			return controller.ApiResult(c, http.StatusUnauthorized, err.Error(), nil)
		}

		if claims.ID != uint32(paramUserId) {
			return controller.ApiResult(c, http.StatusUnauthorized, "Unauthorized", nil)
		}

		return next(c)
	}
}
