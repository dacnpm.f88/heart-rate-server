package mdw

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/dacnpm.f88/heart-rate-server/model"
	"gitlab.com/dacnpm.f88/heart-rate-server/repository"
	"gitlab.com/dacnpm.f88/heart-rate-server/service"
)

var (
	userRepository  model.UserRepository
	userService model.UserService
	authRepository model.AuthRepository
	authService model.AuthService
	resultRepository model.ResultRepository
	resultService model.ResultService
)

func init() {
	userRepository = repository.NewUserRepository()
	userService = service.NewUserService(userRepository)
	authRepository = repository.NewAuthRepository()
	authService = service.NewAuthService(authRepository)
	resultRepository = repository.NewResultRepository()
	resultService = service.NewResultService(resultRepository)
}

func UseMdw(e *echo.Echo) {
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))
}